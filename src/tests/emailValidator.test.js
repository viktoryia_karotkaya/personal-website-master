import 'regenerator-runtime/runtime';
import { expect, assert } from 'chai';
import {validate, validateAsync, validateWithThrow, validateWithLog} from '../js/email-validator.js';
import sinon from 'sinon';

describe('first test', () => {
  it('should return 2', () => {
    expect(2).to.equal(2);
  })
});

describe('Email Validation', () => {
  it('should return false for invalid ending', () => {
    const expected = false;
    const actual = validate('test@test.com');
    expect(actual).to.equal(expected);
  })
  it('should return true for valid ending', () => {
    const expected = true;
    const actual = validate('test@gmail.com');
    expect(actual).to.equal(expected);
  })
  it('should return false for empty string', () => {
    const expected = false;
    const actual = validate('');
    expect(actual).to.equal(expected);
  })
});

describe('Async Email Validation', () => {
  it('should return false for invalid ending', async () => {
    const expected = false;
    const actual = await validateAsync('test@test.com');
    expect(actual).to.equal(expected);
  })
  it('should return true for valid ending', async () => {
    const expected = true;
    const actual = await validateAsync('test@gmail.com');
    expect(actual).to.equal(expected);
  })
  it('should return false for empty string', async () => {    
    const expected = false;
    const actual = await validateAsync('');
    expect(actual).to.equal(expected);
  })
});

describe('Email Validation with throwing error', () => {
  it('should return error for invalid ending', () => {
    // expect(() => validateWithThrow('test@test.com')).to.throw('Email is invalid');
    assert.throw(() => validateWithThrow('test@test.com'), Error, "Email is invalid");
  })
  it('should return true for valid ending', () => {
    const expected = true;
    const actual = validateWithThrow('test@gmail.com');
    expect(actual).to.equal(expected);
  })
  it('should return error for empty string', () => {
    assert.throw(() => validateWithThrow(''), Error, "Email is invalid");
  })
});

describe('Email Validation with Log', () => {
  let log;
  beforeEach( function() {
    log = sinon.spy(console, 'log');
  });
  afterEach( function() {;
    log.restore();
  });
 
  it('log with false for invalid ending', () => {
    // let spy = sinon.spy(console, 'log');
  
    // call the function that needs to be tested
    validateWithLog('test@test.com');
  
    // assert that it was called with the correct value
    assert(log.calledWith(false));
  
    // restore the original function
    // spy.restore();
    // validateWithLog('test@test.com');
    // assert.isTrue(log.calledOnce);
    
  })
  it('log with true for valid ending', () => {
    validateWithLog('test@gmail.com');
    assert(log.calledWith(true));
  })
  it('should return false for empty string', () => {
    validateWithLog('');
    assert(log.calledWith(false));
  })
});
