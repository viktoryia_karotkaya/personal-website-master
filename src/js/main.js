import 'regenerator-runtime/runtime';
import SectionCreator from './join-us-section.js';
import buildSectionCommunity from './community-section.js';
import {validate} from './email-validator.js';
import { API } from "./request-service.js";
import WebsiteSection from './website-section';
import getPerformanceReport from './performance-measure';
import '../styles/style.css';

let loading = false;
const worker = new Worker(new URL('./worker.js', import.meta.url));
worker.onmessage = (e) => {
  console.log(e.data);
}

window.addEventListener('load', getJoinUsSection);
window.addEventListener('load', getCommunity);
window.addEventListener("load", getPerformanceReport);

function getJoinUsSection(){
  const factory = new SectionCreator();
  factory.create('standard');
  const inputEmail = document.querySelector('input[name="email"]');
  const btnSubmit = document.querySelector('input[name="submit"]');
  inputEmail.value = localStorage.getItem('email');
  inputEmail.addEventListener('input', addEmailToLocalStorage);
  const isSubscribed = localStorage.getItem('isSubscribed');
  
  if (isSubscribed === 'true'){
    inputEmail.required = false;
    inputEmail.style.display = 'none';
    btnSubmit.value = 'Unsubscribe';
  }
  const form = document.querySelector('form');
  form.addEventListener('submit', (ev) => {
    event.preventDefault();
    sentDataToWorker(ev);
    if(btnSubmit.value === 'Subscribe'){
      subscribe(ev);
    } else {
      unsubscribe(ev);
    }
  });
  
}

async function getCommunity(){
  let communityList = [];
  performance.mark("getCommunityStart");
  API.get('community')
    .then((data) => {
      performance.mark("getCommunityEnd");
      performance.measure('communitySection', "getCommunityStart", "getCommunityEnd");
      communityList = [...data];
    })
    .catch(err => alert('Something went wrong'))
    .finally(() => {
      buildSectionCommunity(communityList);
    });
}

function addEmailToLocalStorage(event){
  const emailTemplate = event.target.value.trim();
  sentDataToWorker(event);  
  if (emailTemplate.length < 1) return localStorage.setItem('email', '');
  localStorage.setItem('email', emailTemplate);
}

function subscribe(event){
  if (loading) return;
  loading = true;
  const btnSubmit = event.target.elements.submit;
  toggleDisableForButton(btnSubmit);
  const emailInput = event.target.elements.email;
  const { value: emailValue } = event.target.elements.email;
  const isValidEmail = validate(emailValue);
  if (isValidEmail){
    const user = {
      email: emailValue
    };
    API.post('subscribe', user)
      .then((data) => {
        localStorage.setItem('email', emailValue);
        localStorage.setItem('isSubscribed', 'true');
        emailInput.required = false;
        emailInput.style.display = 'none';
        btnSubmit.value = 'Unsubscribe';
      })
      .catch(err => alert('Something went wrong'))
      .finally(() => {
        loading = false;
        toggleDisableForButton(btnSubmit);
      });
  } else {
    alert('Email is not valid');
    loading = false;
    toggleDisableForButton(btnSubmit);
  }
}

function unsubscribe(event){
  if (loading) return;
  loading = true;
  const btnSubmit = event.target.elements.submit;
  toggleDisableForButton(btnSubmit);
  const email = localStorage.getItem('email');
  const user = {
    email: email
  };
  API.post('unsubscribe', user)
    .then((data) => {
      localStorage.setItem('email', '');
      localStorage.setItem('isSubscribed', 'false');
      btnSubmit.value = 'Subscribe';
      const emailInputField = event.target.elements.email;
      emailInputField.style.display = 'inline-block';
      emailInputField.value = '';
      emailInputField.required = false;
    })
    .catch(err => alert('Something went wrong'))
    .finally(() => {
      loading = false;
      toggleDisableForButton(btnSubmit);
    });
}

function toggleDisableForButton(btn){
  btn.disabled = loading;
  if (loading){
    btn.classList.add('app-section__button--disabled');
  } else {
    btn.classList.remove('app-section__button--disabled');
  }
}

function sentDataToWorker(event){
  let eventData = {
    eventType: event.type,
    fieldName: event.target.name
  };
  if (event.type === 'input'){
    eventData.data = event.data;
  }
  if (event.type === 'submit'){
    eventData.method = event.target.method;
  }
  let data = JSON.stringify(eventData);
  worker.postMessage(data);
}
