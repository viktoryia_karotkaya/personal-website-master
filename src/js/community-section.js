export default function buildSectionCommunity(listOfCommunity){
  const section = createElement('section', 'app-section app-section--community');
  const h2 = createElement('h2', 'app-title', [], 'Big community of<br />people like you');
  section.appendChild(h2);
  
  const textH3 = `We’re proud of our products, and we’re really excited<br />when we get feedback from our users.`;
  const h3 = createElement('h3', 'app-subtitle', [], textH3);
  section.appendChild(h3);
  if (listOfCommunity.length > 0){
    const cards = createElement('div', 'app-section--cards');
  
    listOfCommunity.forEach( item => {
      const card = createElement('div', 'app-section--card');
      const avatar = createElement('img', 'card-avatar');
      avatar.src = item.avatar;
      avatar.alt = 'Avatar';
      avatar.width = '150';
      avatar.height = '150';
      card.append(avatar);
    
      const textDescription = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.';
      const cardDescription = createElement('p', 'card-description', [], textDescription);
      card.append(cardDescription);
    
      const fullName = createElement('div', 'card-fullName', [], `${item.firstName} ${item.lastName}`);
      card.append(fullName);
      const position = createElement('div', 'card-position', [], item.position);
      card.append(position);
    
      cards.append(card);
    });
    section.appendChild(cards);
  }
  const siblingSection = document.querySelectorAll('website-section')[1];
  siblingSection.after(section);
  return section;
}

function createElement(elTag, className = undefined, elAttr = [], elHtml = undefined) {
  const elementToAdd = document.createElement(elTag);
  if (className) elementToAdd.className = className;
  if (elAttr.length > 0) {
    elAttr.forEach((item) => {
      elementToAdd.setAttribute(item.name, item.value);
    });
  }
  
  if (elHtml) elementToAdd.innerHTML = elHtml;
  return elementToAdd;
}

