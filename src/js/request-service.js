export const API = {
  baseUrl: 'http://localhost:8080/',
  
  _handleError(_res) {
    if (_res.ok) return _res.status === 204 ? {} : _res.json();
    if (_res.status === 400 || _res.status === 422) {
      return _res.json().then(_res => {
        return Promise.reject(_res.error)
      })
    }
    return Promise.reject(_res.statusText);
  },
  
  get(endpoint) {
    return fetch(`${this.baseUrl}${endpoint}`, {
      method: 'GET'
    })
      .then(this._handleError)
  },
  
  post(endpoint, body) {
    return fetch(`${this.baseUrl}${endpoint}`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(body),
      
    })
      .then(this._handleError)
  }
};
