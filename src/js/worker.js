import 'regenerator-runtime/runtime';
import { API } from "./request-service.js";
let batch = [];

self.onmessage = function(e){
  const eventObj = JSON.parse(e.data);
  batch.push(eventObj);
  if(batch.length >= 5) {
    API.post('analytics/user', batch)
      .then((data) => {
        batch.length = 0;
        self.postMessage(`Data sent to server successfully: ${data.success}`);
      })
      .catch(err => {        
        self.postMessage(`Something went wrong : ${err}`);
      });
  } else {
    self.postMessage('Data is placed into batch')
  }
  
}
