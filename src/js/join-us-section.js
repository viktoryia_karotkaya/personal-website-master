// factory for creating new section
export default class SectionCreator {
  create(type) {
    if (type === 'standard') return new JoinUsSectionStandard();
    if (type === 'advanced') return new JoinUsSectionAdvanced();
  }
}

// abstract class JoinUsSection, with singleton pattern: we check if constructor
// has instance.
class JoinUsSection {
  constructor(title, btnName) {
    if (JoinUsSection.exist) {
      return JoinUsSection.instance;
    }
    this.title = title;
    this.btnName = btnName;
    this.section = this.createSection();
    JoinUsSection.instance = this;
    JoinUsSection.exist = true;
  }
  
  // method for new section creation
  createSection() {
    // eslint-disable-net-line max-len
    const section = this.createElement('section', 'app-section app-section--join-program');
    const h2 = this.createElement('h2', 'app-title', [], this.title);
    section.id = 'joinOurProgram';
    this.addElementToNode(section, h2);
    
    const h3 = this.createElement('h3', 'app-subtitle', [], 'Sed do eiusmod tempor incididunt<br />ut labore et dolore magna aliqua.');
    this.addElementToNode(section, h3);
    
    const form = this.createElement('form', undefined, []);
    const inputEmail = this.createElement('input', 'app-section__input', [
      {name: 'type', value: 'email'},
      {name: 'name', value: 'email'},
      {name: 'placeholder', value: 'Email'},
      {name: 'required', value: 'true'}
    ]);
    this.addElementToNode(form, inputEmail);
    const submitBtn = this.createElement('input', 'app-section__button app-section__button--subscribe', [
      {name: 'type', value: 'submit'},
      {name: 'name', value: 'submit'},
      {name: 'value', value: this.btnName}
    ]);
    this.addElementToNode(form, submitBtn);
    this.addElementToNode(section, form);
    
    const footer = document.getElementsByTagName('footer')[0];
    footer.before(section);
    return section;
  }
  
  // method for adding element to parent node
  addElementToNode(parent, el) {
    parent.appendChild(el);
  }
  
  // method for creation DOM element
  createElement(elTag, className = undefined, elAttr = [], elHtml = undefined) {
    const elementToAdd = document.createElement(elTag);
    if (className) elementToAdd.className = className;
    if (elAttr.length > 0) {
      elAttr.forEach((item) => {
        elementToAdd.setAttribute(item.name, item.value);
      });
    }
    
    if (elHtml) elementToAdd.innerHTML = elHtml;
    return elementToAdd;
  }
  
  // method helps remove this section from the DOM, aslo it sends to this class information
  // there is no instance of this class
  remove() {
    this.section.remove();
    JoinUsSection.exist = false;
  }
}

// class for standard section
class JoinUsSectionStandard extends JoinUsSection {
  constructor(title = 'Join Our Program', btnName = 'Subscribe') {
    super(title, btnName);
  }
}

// class for advanced section
class JoinUsSectionAdvanced extends JoinUsSection {
  constructor(title = 'Join Our Advanced Program', btnName = 'Subscribe to Advanced Program') {
    super(title, btnName);
  }
}


