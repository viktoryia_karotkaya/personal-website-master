import 'regenerator-runtime/runtime';
import { API } from "./request-service.js";

export default function getPerformanceReport(){
  const reports = [];
  // memory info
  const memoryReport = perfReport("Memory-Info",
    performance.memory.usedJSHeapSize);
  reports.push(memoryReport);
  // the page load speed
  const navEntries = performance.getEntriesByType("navigation");
  navEntries.forEach( entry => {
    const loadSpeed = entry.domComplete - entry.fetchStart;
    const report = perfReport("Load-Speed",
      loadSpeed);
    reports.push(report)
  });
  //the performance of fetcing the information for the Community section
  const userObserver = new PerformanceObserver(list => {
    list.getEntries().forEach(entry => {
      const report = perfReport("Fetch-Community-Section-Data",
        entry.duration);
      reports.push(report);
      sendReportToServer(reports);          
    })
  })
  userObserver.observe({entryTypes: ["measure"]});
}

function perfReport(type, data){
  return {
    type: type,
    data: data
  }  
}

function sendReportToServer(data){
  let headers = {
    type: 'application/json'
  };
  let blob = new Blob([JSON.stringify(data)], headers);
  navigator.sendBeacon('http://localhost:8080/analytics/performance', blob);
}
