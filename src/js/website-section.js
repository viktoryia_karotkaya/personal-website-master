import GlobalStyles from '../styles/style.css';

export default class WebsiteSection extends HTMLElement {
  constructor() {
    super();
    var shadowRoot = this.attachShadow({mode: 'open'});
    let content = '';
    let cssSttyles = `
      <style>
            :host {
              font: normal 16px 'Source Sans Pro', sans-serif, Arial;
            }
            h1,
            h2,
            h3,
            h4,
            h5,
            h6,
            button {
              font-family: 'Oswald', sans-serif;
            }
            .app-title {
              font-size: 3.25rem;
              line-height: 3.20rem;
              text-align: center;
            }
            
            .app-subtitle {
              font-size: 1.50rem;
              line-height: 1.625rem;
              font-weight: 300;
              text-align: center;
              font-family: 'Source Sans Pro', sans-serif, Arial;
            }
          </style>
    `;
    const title = this.attributes.title.value;
    const description = this.attributes.description.value;
    
    switch (this.attributes.sectionName.value) {
      case 'logo-section':
        content = `          
          <slot name="logo"></slot>
          <h1 class="app-title">${title}</h1>
          <h2 class="app-subtitle">${description}</h2>
        `;
        break;
      case 'article-section':
        content = `
          <h2 class="app-title">${title}</h2>
          <h3 class="app-subtitle">${description}</h3>
          <slot></slot>
        `;
        break;
      case 'learn-section':
        content = `
          <h2 class="app-title">${title}</h2>
          <slot name="button"></slot>          
          <h3 class="app-subtitle">${description}</h3>
        `;
        break;
    }
    shadowRoot.innerHTML = cssSttyles + content;
  } 
}

window.customElements.define('website-section', WebsiteSection);

