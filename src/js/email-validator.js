const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

// eslint-disable-next-line require-jsdoc
export const validate = email => {
  if (email.length === 0) return false;
  const ending = email.split('@')[1].toLowerCase();
  return VALID_EMAIL_ENDINGS.includes(ending);
}

// const regexp = /(gmail.com|outlook.com|yandex.ru)$/i;
// export const validate = (email) => Boolean(email.match(regexp));

export const validateAsync = (email) => {
  return new Promise(resolve => {
    if (email.length === 0) return resolve(false);
    const ending = email.split('@')[1].toLowerCase();
    resolve(VALID_EMAIL_ENDINGS.includes(ending));
  })
}

export const validateWithThrow = (email) => {  
  if (email.length === 0) throw new Error ('Email is invalid');
  const ending = email.split('@')[1].toLowerCase();
  if (VALID_EMAIL_ENDINGS.includes(ending)){
    return true;
  } else {
    throw new Error ('Email is invalid');
  }
}

export const validateWithLog = (email) => {
  let isValid = false;
  if (email.length === 0) {
    console.log(isValid);
    return isValid;
  }
  const ending = email.split('@')[1].toLowerCase();
  isValid = VALID_EMAIL_ENDINGS.includes(ending);
  console.log(isValid);
  return isValid;
}

