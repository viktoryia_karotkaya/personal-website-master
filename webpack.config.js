const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');

const config = {
  entry: {
    main: path.resolve(__dirname, './src/js/main.js')
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'build')
  },
  experiments: {
    asset: true
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.css$/i,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader:'css-loader'
          }],

      },
      {
        test: /\.svg$/,
        type: 'asset',
        use: 'svgo-loader'
      },
      {
        test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
        type: 'asset/resource',
      },      
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "src/index.html",
    }),
    new CleanWebpackPlugin()
  ],
};

module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.devtool = "inline-source-map";
    config.mode = "development";
    config.devServer = {
      proxy: {
        '/': 'http://localhost:3000'
      },
      // headers: {
      //   'Cache-Control': 'public, max-age=31536000',
      // },
      contentBase: path.join(__dirname, 'src'),
      compress: true,
      port: 8080,
      open: true,
      hot: true,
      watchContentBase: true
    };
  }

  if (argv.mode === 'production') {
    config.mode = "production";
    config.optimization = {
      minimize: true
    }
  }

  return config;
};
