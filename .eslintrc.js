module.exports = {
  'env': {
    'browser': true,
    'es2021': true,
  },
  'extends': [
    'google',
  ],
  'parserOptions': {
    'ecmaVersion': 12,
    'sourceType': 'module',
  },
  'rules': {
  },
  'overrides': [
    {
      'files': ['*.js'],
      'rules': {
        'max-len': 'off',
        'no-trailing-spaces': 'off',
        'require-jsdoc': 'off',
        'comma-dangle': 'off',
        'space-before-blocks': 'off',
        'no-inline-comments': 'off',
        'no-console': 'off',
        'no-empty': 'error',
        'no-unreachable': 'error',
        'no-setter-return': 'error',
        'indent': 'off',
        'arrow-parens': 'off'
      }
    }
    ]
};
